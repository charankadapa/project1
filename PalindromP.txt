begin
	numeric num,temp,rev,rem=0;
	accept five digit num;
	temp = num;
	while(num!=0):
		rem = num%10;
		rev = rem + rev*10;
		num = num/10;
	end loop
	if(rev == temp):
		Display "Palindrome";
	else:
		Display "Not a Palindrome";
	end if
end